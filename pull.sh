
#http://stackoverflow.com/questions/6335681/git-how-do-i-get-the-latest-version-of-my-code
git fetch origin
git --git-dir=core/.git fetch origin
git --git-dir=would-you-rather/.git fetch origin
git --git-dir=would-you-rather/~commons/.git fetch origin

# reset --hard SE LIA y acaba mezclando repositorios!
#git reset --hard origin/master
#git --git-dir=core/.git reset --hard origin/master
#git --git-dir=would-you-rather/.git reset --hard origin/master
#git --git-dir=would-you-rather/~commons/.git reset --hard origin/master

#credentials store
git config credential.helper store
git --git-dir=core/.git config credential.helper store
git --git-dir=would-you-rather/.git config credential.helper store
git --git-dir=would-you-rather/~commons/.git config credential.helper store

#pull
git pull
git --git-dir=core/.git pull
git --git-dir=would-you-rather/.git pull
git --git-dir=would-you-rather/~commons/.git pull
