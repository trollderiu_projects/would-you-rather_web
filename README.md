CLONE: (and give apache control!)
```
git clone https://trollderiu@bitbucket.org/imageVote/would-you-rather_web.git wouldyourather
git clone https://trollderiu@bitbucket.org/imageVote/core.git wouldyourather/core
git clone https://trollderiu@bitbucket.org/imageVote/would-you-rather.git wouldyourather/would-you-rather
git clone https://trollderiu@bitbucket.org/imageVote/imagevote_public.git wouldyourather/would-you-rather/~commons
chown -R www-data:www-data wouldyourather

cp -R wouldyourather wouldyourather_builder
chown -R www-data:www-data wouldyourather_builder
```
PULL:
```
sh pull.sh
```

PRE-LAUNCH TEST GUIDE:

on builder (android browser):
//- run minify.bat
- git commit + push all in "would-you-rather"
- git pull all on "var/www/html/wouldyourather_builder" (WARNING!)

- Clean cache
- Load "would-you-rather-builder.tk" (select language + quit tutorial)
- Fill and Share new votation
- Click some shared poll "would-you-rather-builder.tk/<KEY>"
- Check click answer callback works on 'Play'

on Android (android browser)
- remove android app
- Click some shared poll "would-you-rather-builder.tk/<KEY>"
- click option to share it 
- click ok on use app and check redirection
- go back, and click again and check image creation

on Android (app):
- git pull all in "assets"
- Android Studio build and run

- Click 'Play' and 'Share' (check image + link)
- Vote and 'Share' again (memorize votations numbers)
- Change language and 'Share' again
- Change original language and check number votations are correct
- Click the link and open with app
- Click same answer again
- Click SKIP
- Move forward and backwards (check there is not jumps)
- Click the link and open with browser

- Go back, clicking New
- Click "show my polls" + "hide my polls"
- Click some poll
- Vote
- Click back (is poll updated?)

//on iPohne:
//TODO

upload:
- check core update will not affect old android/iphone version users

- git push/pull android version
- Change manifest version
- Build signed apk
- Upload in "play.google.com/apps/publish"

- git pull on production server

- git push android project
- Create branch in Bitbucket with the version 0.* for: "would-you-rather_android", "android", "would-you-rather_web", "would-you-rather", "imagevote_public", "core"
