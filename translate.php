<?php

require 'load.php';

$public = "../would-you-rather/";

//LANGUAGES
$targetFile = "en.js";
$paths = array(
    "~lang",
    "~modules/tutorial/lang",
    "~commons/lang",
    "~commons/modules/like/lang",
    "~commons/modules/polls/lang",
    "~commons/modules/rate/lang",
    "~commons/modules/report/lang",
    "~commons/modules/tutorial/lang",
    "~commons/modules/config/lang"
);

include 'translationUpdate.php';


////BLACKLIST
//$targetFile = "en.json";
//$paths = array(
//    "~commons/modules/blacklist/json"
//);
//
//include 'translationJSON.php';
